package entities

import (
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"encoding/json"
	"strconv"
	"github.com/davecgh/go-spew/spew"
	"kodix-contest/database"
	"log"
	"math"
)

type Model struct {
	Id    uint   `json:"id"`
	Name  string `json:"name"`
	Brand string `json:"brand"`
	Year  uint   `json:"year"`
}

func (m *Model) Validate() bool {
	valid := true
	if m.Id <= 0 {
		valid = false
	}
	if m.Name == "" {
		valid = false
	}
	if m.Brand == "" {
		valid = false
	}
	if m.Year <= 0 {
		valid = false
	}
	return valid
}

func (m *Model) Create(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	db := database.GetStorage()
	raw, err := ioutil.ReadAll(ctx.Request.Body)
	if err == nil {
		err = json.Unmarshal(raw, m)
		if err == nil {
			if m.Validate() {
				tx := db.Txn(true)
				err = tx.Insert("models", m)
				if err == nil {
					tx.Commit()
					code = 200
				} else {
					tx.Abort()
					spew.Print(err.Error())
				}
			}
		} else {
			spew.Println(err.Error())
		}
	} else {
		ctx.JSON(code, resp)
	}
}

func (m *Model) Get(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	db := database.GetStorage()
	id, err := strconv.Atoi(ctx.Params.ByName("id"))
	if err == nil {
		tx := db.Txn(false)
		defer tx.Abort()
		raw, err := tx.First("models", "id", uint(id))
		if err == nil {
			if raw == nil {
				code = 404
			} else {
				code = 200
				resp = raw
			}
		} else {
			spew.Println(err.Error())
		}
	} else {
		spew.Println(err.Error())
	}
	ctx.JSON(code, resp)
}

func (m *Model) Update(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	db := database.GetStorage()
	id, err := strconv.Atoi(ctx.Params.ByName("id"))
	if err == nil {
		if id > 0 {
			tx := db.Txn(false)
			raw, err := tx.First("models", "id", uint(id))
			tx.Abort()
			if err == nil {
				if raw != nil {
					m = raw.(*Model)
					upd := Model{}
					body, err := ioutil.ReadAll(ctx.Request.Body)
					if err == nil {
						err = json.Unmarshal(body, &upd)
						if upd.Name != "" {
							m.Name = upd.Name
						}
						if upd.Brand != "" {
							m.Brand = upd.Brand
						}
						if upd.Year >= 0 {
							m.Year = upd.Year
						}
						tx = db.Txn(true)
						err = tx.Delete("models", raw)
						if err == nil {
							err = tx.Insert("models", m)
							if err == nil {
								code = 200
							}
						}
					} else {
						spew.Println(err.Error())
					}
				} else {
					code = 404
				}
			}
		}
	} else {
		log.Println(err.Error())
	}
	ctx.JSON(code, resp)
}

func (m *Model) GetMark(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	id, err := strconv.Atoi(ctx.Params.ByName("id"))
	db := database.GetStorage()
	if err == nil {
		tx := db.Txn(false)
		raw, err := tx.First("models", "id", uint(id))
		tx.Abort()
		if err == nil {
			if raw != nil {
				parse := true
				from := uint(0)
				if ctx.Query("fromDate") != "" {
					fromtmp, err := strconv.Atoi(ctx.Query("fromDate"))
					if err != nil {
						parse = false
					} else {
						from = uint(fromtmp)
					}
				}
				to := ^uint(0)
				if ctx.Query("fromDate") != "" {
					totmp, err := strconv.Atoi(ctx.Query("toDate"))
					if err != nil {
						parse = false
					} else {
						to = uint(totmp)
					}
				}
				sex := ""
				if ctx.Query("sex") != "" {
					if ctx.Query("sex") == "male" || ctx.Query("sex") == "female" {
						sex = ctx.Query("sex")
					} else {
						parse = false
					}
				}
				if parse == true {
					tx = db.Txn(false)
					rows, err := tx.Get("reviews", "model", uint(id))
					if err == nil {
						var mark float64
						mark = 0
						sum := uint(0)
						cnt := 0
						success := true
						for {
							row := rows.Next()
							if row == nil {
								break
							} else {
								rev := row.(Review)
								if rev.Created > from && rev.Created < to {
									if sex != "" {
										itx := db.Txn(false)
										urow, err := itx.First("users", "id", rev.User)
										if err == nil && urow != nil {
											if sex == urow.(*User).Sex {
												sum += rev.Mark
												cnt += 1
											}
										} else {
											success = false
										}
									} else {
										sum += rev.Mark
										cnt += 1
									}
								}
							}
							if success == false {
								break
							}
						}
						if success == true {
							mark = float64(sum) / float64(cnt)
							var round float64
							pow := math.Pow(10, float64(5))
							digit := pow * mark
							round = math.Ceil(digit)
							mark = round / pow
							resp = struct {
								Mark float64 `json:"mark"`
							}{Mark: mark}
							code = 200
						}
					}
				}
			} else {
				code = 404
			}
		}
	}
	ctx.JSON(code, resp)
}
