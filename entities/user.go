package entities

import (
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"encoding/json"
	"github.com/davecgh/go-spew/spew"
	"kodix-contest/database"
	"strconv"
	"log"
)

type User struct {
	Id        uint   `json:"id"`
	Email     string `json:"email"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
	Sex       string `json:"sex"`
	BirthDate uint   `json:"birth_date"`
}

func (u *User) Validate() bool {
	valid := true
	if u.Id <= 0 {
		valid = false
	}
	if u.Email == "" {
		valid = false
	}
	if u.FirstName == "" {
		valid = false
	}
	if u.LastName == "" {
		valid = false
	}
	if u.Sex != "male" && u.Sex != "female" {
		valid = false
	}
	if u.BirthDate <= 0 {
		valid = false
	}
	return valid
}

func (u *User) Create(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	db := database.GetStorage()
	raw, err := ioutil.ReadAll(ctx.Request.Body)
	if err == nil {
		err = json.Unmarshal(raw, u)
		if err == nil {
			if u.Validate() {
				tx := db.Txn(true)
				err = tx.Insert("users", u)
				if err == nil {
					tx.Commit()
					code = 200
				} else {
					tx.Abort()
					spew.Print(err.Error())
				}
			}
		} else {
			spew.Println(err.Error())
		}
	} else {
		ctx.JSON(code, resp)
	}
}

func (u *User) Get(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	db := database.GetStorage()
	id, err := strconv.Atoi(ctx.Params.ByName("id"))
	if err == nil {
		tx := db.Txn(false)
		defer tx.Abort()
		raw, err := tx.First("users", "id", uint(id))
		if err == nil {
			if raw == nil {
				code = 404
			} else {
				code = 200
				resp = raw
			}
		} else {
			spew.Println(err.Error())
		}
	} else {
		spew.Println(err.Error())
	}
	ctx.JSON(code, resp)
}

func (u *User) Update(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	db := database.GetStorage()
	id, err := strconv.Atoi(ctx.Params.ByName("id"))
	if err == nil {
		if id > 0 {
			tx := db.Txn(false)
			raw, err := tx.First("users", "id", uint(id))
			tx.Abort()
			if err == nil {
				if raw != nil {
					u = raw.(*User)
					upd := User{}
					body, err := ioutil.ReadAll(ctx.Request.Body)
					if err == nil {
						err = json.Unmarshal(body, &upd)
						if upd.Email != "" {
							u.Email = upd.Email
						}
						if upd.FirstName != "" {
							u.FirstName = upd.FirstName
						}
						if upd.FirstName != "" {
							u.FirstName = upd.FirstName
						}
						if upd.LastName != "" {
							u.LastName = upd.LastName
						}
						if upd.BirthDate >= 0 {
							u.BirthDate = upd.BirthDate
						}
						if upd.Sex == "male" || upd.Sex == "female" {
							u.Sex = upd.Sex
						}
						tx = db.Txn(true)
						err = tx.Delete("users", raw)
						if err == nil {
							err = tx.Insert("users", u)
							if err == nil {
								code = 200
							}
						}
					} else {
						spew.Println(err.Error())
					}
				} else {
					code = 404
				}
			}
		}
	} else {
		log.Println(err.Error())
	}
	ctx.JSON(code, resp)
}

func (u *User) Reviews(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	id, err := strconv.Atoi(ctx.Params.ByName("id"))
	if err == nil {
		db := database.GetStorage()
		tx := db.Txn(false)
		raw, err := tx.First("users", "id", uint(id))
		tx.Abort()
		if err == nil {
			if raw != nil {
				tx = db.Txn(false)
				defer tx.Abort()
				rows, err := tx.Get("reviews", "user", uint(id))
				if err == nil {
					parse := true
					from := uint(0)
					if ctx.Query("fromDate") != "" {
						fromtmp, err := strconv.Atoi(ctx.Query("fromDate"))
						if err != nil {
							parse = false
						} else {
							from = uint(fromtmp)
						}
					}
					to := ^uint(0)
					if ctx.Query("fromDate") != "" {
						totmp, err := strconv.Atoi(ctx.Query("toDate"))
						if err != nil {
							parse = false
						} else {
							to = uint(totmp)
						}
					}
					if parse == true {
						if err == nil {
							revs := Reviews{}
							for {
								row := rows.Next()
								if row == nil {
									break
								} else {
									rev := row.(Review)
									if rev.Created > from && rev.Created < to {
										revs.Reviews = append(revs.Reviews, rev)
									}
								}
							}
							resp = revs
							code = 200
						}
					}
				}
			} else {
				code = 404
			}
		}
	} else {
		log.Println(err.Error())
	}
	ctx.JSON(code, resp)
}
