package entities

import (
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"encoding/json"
	"strconv"
	"github.com/davecgh/go-spew/spew"
	"kodix-contest/database"
	"log"
)

type Review struct {
	Id      uint `json:"id"`
	User    uint `json:"user"`
	Model   uint `json:"model"`
	Created uint `json:"created"`
	Mark    uint `json:"mark"`
}

type Reviews struct {
	Reviews []Review `json:"reviews"`
}

func (r *Review) Validate() bool {
	valid := true
	db := database.GetStorage()
	if r.Id <= 0 {
		valid = false
	}
	if r.User <= 0 {
		valid = false
	} else {
		tx := db.Txn(false)
		raw, err := tx.First("users", "id", r.User)
		if err == nil || raw == nil {
			valid = false
		}
		tx.Abort()
	}
	if r.Model <= 0 {
		valid = false
	} else {
		tx := db.Txn(false)
		raw, err := tx.First("models", "id", r.Model)
		if err == nil || raw == nil {
			valid = false
		}
		tx.Abort()
	}
	if r.Created <= 0 {
		valid = false
	}
	if r.Mark < 0 || r.Mark > 5 {
		valid = false
	}
	return valid
}

func (r *Review) Create(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	db := database.GetStorage()
	raw, err := ioutil.ReadAll(ctx.Request.Body)
	if err == nil {
		err = json.Unmarshal(raw, r)
		if err == nil {
			if r.Validate() {
				tx := db.Txn(true)
				err = tx.Insert("reviews", r)
				if err == nil {
					tx.Commit()
					code = 200
				} else {
					tx.Abort()
					spew.Print(err.Error())
				}
			}
		} else {
			spew.Println(err.Error())
		}
	} else {
		ctx.JSON(code, resp)
	}
}

func (r *Review) Get(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	db := database.GetStorage()
	id, err := strconv.Atoi(ctx.Params.ByName("id"))
	if err == nil {
		tx := db.Txn(false)
		defer tx.Abort()
		raw, err := tx.First("reviews", "id", uint(id))
		if err == nil {
			if raw == nil {
				code = 404
			} else {
				code = 200
				resp = raw
			}
		} else {
			spew.Println(err.Error())
		}
	} else {
		spew.Println(err.Error())
	}
	ctx.JSON(code, resp)
}

func (r *Review) Update(ctx *gin.Context) {
	var resp interface{}
	resp = struct{}{}
	code := 400
	db := database.GetStorage()
	id, err := strconv.Atoi(ctx.Params.ByName("id"))
	if err == nil {
		if id > 0 {
			tx := db.Txn(false)
			raw, err := tx.First("reviews", "id", uint(id))
			tx.Abort()
			if err == nil {
				if raw != nil {
					r = raw.(*Review)
					upd := Review{}
					body, err := ioutil.ReadAll(ctx.Request.Body)
					if err == nil {
						err = json.Unmarshal(body, &upd)
						if upd.User >= 0 {
							r.User = upd.User
						}
						if upd.Model >= 0 {
							r.Model = upd.Model
						}
						if upd.Created >= 0 {
							r.Created = upd.Created
						}
						if upd.Mark < 5 && upd.Mark >= 0 {
							r.Mark = upd.Mark
						}
						tx = db.Txn(true)
						err = tx.Delete("reviews", raw)
						if err == nil {
							err = tx.Insert("reviews", r)
							if err == nil {
								code = 200
							}
						}
					} else {
						spew.Println(err.Error())
					}
				} else {
					code = 404
				}
			}
		}
	} else {
		log.Println(err.Error())
	}
	ctx.JSON(code, resp)
}
