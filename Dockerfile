FROM golang:1.9 as builder

RUN mkdir /go/src/kodix-contest
WORKDIR /go/src/kodix-contest

COPY . .

RUN go get -v
RUN go build -v

FROM ubuntu:16.04

ENV DEBIAN_FRONTEND=noninteractive

WORKDIR /root/
COPY . .

COPY --from=builder /go/src/kodix-contest/kodix-contest ./kodix-contest

RUN chmod 755 kodix-contest

ENTRYPOINT ["/root/kodix-contest"]
