package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"kodix-contest/entities"
	"kodix-contest/database"
	"regexp"
)

func main() {
	database.Init()
	r := gin.Default()
	r.Use(func(ctx *gin.Context) {
		h := ctx.Request.Header.Get("Content-Type")
		match, _ := regexp.Match("application/json", []byte(h))
		if match == false {
			ctx.AbortWithStatus(400)
		}
	})
	r.GET("/ping", func(ctx *gin.Context) {
		ctx.JSON(200, struct {
			Msg string
		}{Msg: "pong"})
	})
	userRouter := r.Group("/user")
	{
		userRouter.GET("/:id", func(ctx *gin.Context) {
			item := entities.User{}
			item.Get(ctx)
		})
		userRouter.POST("/", func(ctx *gin.Context) {
			item := entities.User{}
			item.Create(ctx)
		})
		userRouter.POST("/:id", func(ctx *gin.Context) {
			item := entities.User{}
			item.Update(ctx)
		})
		userRouter.GET("/:id/reviews", func(ctx *gin.Context) {
			item := entities.User{}
			item.Reviews(ctx)
		})
	}
	modelRouter := r.Group("/model")
	{
		modelRouter.GET("/:id", func(ctx *gin.Context) {
			item := entities.Model{}
			item.Get(ctx)
		})
		modelRouter.POST("/", func(ctx *gin.Context) {
			item := entities.Model{}
			item.Create(ctx)
		})
		modelRouter.POST("/:id", func(ctx *gin.Context) {
			item := entities.Model{}
			item.Update(ctx)
		})
		modelRouter.GET("/:id/mark", func(ctx *gin.Context) {
			item := entities.Model{}
			item.GetMark(ctx)
		})
	}
	reviewRouter := r.Group("/review")
	{
		reviewRouter.GET("/:id", func(ctx *gin.Context) {
			item := entities.Review{}
			item.Get(ctx)
		})
		reviewRouter.POST("/", func(ctx *gin.Context) {
			item := entities.Review{}
			item.Create(ctx)
		})
		reviewRouter.POST("/:id", func(ctx *gin.Context) {
			item := entities.Review{}
			item.Update(ctx)
		})
	}
	err := r.Run(":80")
	if err != nil {
		fmt.Println(err.Error())
	}
}
